"""tests URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os

from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.views.static import serve

from example.views import IndexView, UserInfoView


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("realtime_chat.urls")),
    path("", IndexView.as_view(), name="index"),
    path("api/v1/user/info", UserInfoView.as_view()),
    re_path(
        r"^static/(?P<path>.*)$",
        serve,
        {"document_root": settings.STATIC_ROOT},
    ),
    re_path(
        r"^dmedia/(?P<path>.*)$", serve, {"document_root": settings.MEDIA_ROOT}
    ),
    re_path(
        r"^media/(?P<path>.*)$",
        serve,
        {"document_root": os.path.join(settings.VUE_ROOT, "media")},
    ),
    re_path(
        r"^img/(?P<path>.*)$",
        serve,
        {"document_root": os.path.join(settings.VUE_ROOT, "img")},
    ),
    re_path(
        r"^js/(?P<path>.*)$",
        serve,
        {"document_root": os.path.join(settings.VUE_ROOT, "js")},
    ),
    re_path(
        r"^css/(?P<path>.*)$",
        serve,
        {"document_root": os.path.join(settings.VUE_ROOT, "css")},
    ),
    re_path(
        r"^fonts/(?P<path>.*)$",
        serve,
        {"document_root": os.path.join(settings.VUE_ROOT, "fonts")},
    ),
]


from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


info = openapi.Info(
    title="Realtime chat",
    default_version="v1",
    description="API description",
    contact=openapi.Contact(email="cyberbudy@gmal.com"),
    license=openapi.License(name="MIT License"),
)

schema_view = get_schema_view(
    info,
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns += [
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc/$",
        schema_view.with_ui("redoc", cache_timeout=0),
        name="schema-redoc",
    ),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
