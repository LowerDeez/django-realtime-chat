import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import VueNativeSock from "vue-native-websocket";

Vue.config.productionTip = false;

Vue.use(VueNativeSock, `ws://${window.location.host}/chat/ws/`, {
  reconnection: true,
  reconnectionAttempts: 5,
  reconnectionDelay: 3000,
  format: "json"
});

new Vue({
  vuetify,
  render: h => h(App)
}).$mount("#app");
