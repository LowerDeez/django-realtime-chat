export const store = {
  state: {
    userInfo: {
      id: null,
      email: null,
      username: null
    },
    selectedSearchUsers: [],
    conversation: {},
    conversations: []
  },
  updateUserInfo(userInfo) {
    this.userInfo = userInfo;
  },
  setSearchedUser(users) {
    this.selectedSearchUsers = users;
  },
  setConversation(conversation) {
    this.conversation = conversation;
  },
  addConversation(conversation) {
    if (!this.conversations) {
      this.conversations = [conversation];
    } else {
      this.conversations.push(conversation);
    }
  },
  addConversations(conversations) {
    if (!this.conversations) {
      this.setConversations(conversations);
      return;
    }
    this.conversations.push(...conversations);
  },
  setConversations(conversations) {
    if (!this.conversations) {
      this.conversations = conversations;
    } else {
      this.conversations.splice(0, this.conversations.length);
      this.conversations.push(...conversations);
    }
  }
};
