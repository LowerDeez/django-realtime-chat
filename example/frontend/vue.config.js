const BundleTracker = require("webpack-bundle-tracker");

module.exports = {
  // publicPath: `http://${process.env.VUE_APP_SERVER_URL}:8080`,
  publicPath: `http://localhost:8080`,
  outputDir: "static",

  // publicPath: "/",
  // outputDir: "static",

  chainWebpack: config => {
    config.optimization.splitChunks(false);

    config.plugin("BundleTracker").use(BundleTracker, [
      {
        filename: "../frontend/webpack-stats.json"
      }
    ]);

    config.resolve.alias.set("__STATIC__", "static");

    config.devServer
      .public("http://0.0.0.0:8080")
      .host("0.0.0.0")
      .port(8080)
      .hotOnly(true)
      .watchOptions({
        poll: 1000
      })
      .https(false)
      .headers({
        "Access-Control-Allow-Origin": ["*"]
      });
  }
};
