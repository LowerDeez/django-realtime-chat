import csv

import pytest
from django.contrib.auth import get_user_model
from django.db import transaction

from realtime_chat.const import MESSAGE_STATUSES
from realtime_chat.models import Conversation, Interlocutor, Message, MessageStatus


@pytest.fixture()
@pytest.mark.django_db()
def test_users(n=5):
    UserModel = get_user_model()
    UserModel.objects.all().delete()

    with transaction.atomic():
        users = [
            UserModel.objects.create(username=f'ad{_}', email=f'ad{_}@ad.ad')
            for _ in range(n)
        ]

    return users


@pytest.fixture()
@pytest.mark.django_db(transaction=True)
def test_conversation():
    UserModel = get_user_model()

    with transaction.atomic():
        user = UserModel.objects.create(username=f'ad', email=f'ad@ad.ad')
        user1 = UserModel.objects.create(username=f'ad1', email=f'ad1@ad.ad')
        conversation = Conversation.objects.create(owner=user)
        interlocutors = [
            Interlocutor.objects.create(user=user, conversation=conversation),
            Interlocutor.objects.create(user=user1, conversation=conversation, unread=1),
        ]
        message = Message.objects.create(
            author=user,
            conversation=conversation,
            text="text"
        )
        MessageStatus.objects.create(
            interlocutor=interlocutors[1],
            user=user1,
            message=message,
            status=MESSAGE_STATUSES.created
        )

    return {
        'conversation': conversation,
        'interlocutors': interlocutors,
        'users': [user, user1],
        'message': message
    }


@pytest.fixture()
def csv_file():
    try:
        myfile = open('test.csv', 'w')
        wr = csv.writer(myfile)
        wr.writerow(('Paper ID','Paper Title', 'Authors'))
        wr.writerow(('1','Title1', 'Author1'))
        wr.writerow(('2','Title2', 'Author2'))
        wr.writerow(('3','Title3', 'Author3'))
    finally:
        myfile.close()

    return myfile
