from io import StringIO

import pytest
from django.core.management import call_command
from django.contrib.auth import get_user_model

from realtime_chat.models import UserStatus


@pytest.mark.django_db
def test_init_chat_command():
    UserModel = get_user_model()
    UserModel.objects.bulk_create(
        [
            UserModel(username="test", email="test@ad.ad"),
            UserModel(username="test1", email="test1@ad.ad"),
        ]
    )
    assert 0 == UserStatus.objects.count()

    out = StringIO()
    call_command("init_chat", stdout=out)
    assert 2 == UserStatus.objects.count()
