import pytest
from django.contrib.auth import get_user_model

from realtime_chat.models import UserStatus


@pytest.mark.django_db
def test_user_status_created():
    # Create works
    UserModel = get_user_model()
    user = UserModel.objects.create(username="ad", email="ad@ad.ad")
    assert 1 == UserStatus.objects.filter(user=user).count()

    # Save also works fine
    user = UserModel(username="admin", email="admin@ad.ad")
    user.save()
    assert 1 == UserStatus.objects.filter(user=user).count()

    # But bulk_create don't
    UserModel.objects.bulk_create(
        [
            UserModel(username="test", email="test@ad.ad"),
            UserModel(username="test1", email="test1@ad.ad"),
        ]
    )
    assert 4 != UserStatus.objects.count()
