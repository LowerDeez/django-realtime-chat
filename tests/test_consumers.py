from asyncio import sleep

import pytest
from asgiref.sync import sync_to_async
from channels.testing import WebsocketCommunicator
from django.contrib.auth import get_user_model

import realtime_chat.consumers
import realtime_chat.conf
import realtime_chat.services
from realtime_chat.consumers import ChatConsumer


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_chat_consumers_invalid_data(test_users):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_users[0]

    connected, _ = await communicator.connect()
    assert connected

    await communicator.send_json_to({
        "some_key": "some_value"
    })
    response = await communicator.receive_json_from()
    assert response.get('type') == "chat.error"


    await communicator.send_json_to({
        "type": "invalid_type"
    })
    response = await communicator.receive_json_from()
    assert response.get('type') == "chat.error"

    with pytest.raises(ValueError):
        await communicator.send_to(text_data="Invalid data")
        response = await communicator.receive_json_from()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_user_status_changed_with_notification(test_conversation):
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = True

    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_conversation['users'][0]

    connected, _ = await communicator.connect()
    assert connected
    await sleep(1)
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_conversation['users'][1]

    connected1, _ = await communicator1.connect()
    assert connected1
    await sleep(1)

    response1 = await communicator.receive_json_from(1)
    assert response1.get('type') == "chat.user.changed"
    assert response1.get('body').get('newStatus') == "online"
    assert response1.get('body').get('userId') == test_conversation['users'][1].id

    await communicator.disconnect()

    response2 = await communicator1.receive_json_from()
    assert response2.get('type') == "chat.user.changed"

    if response2.get('body').get('newStatus') == "offline":
        assert response2.get('body').get('newStatus') == "offline"
    else:
        assert response2.get('body').get('newStatus') == "online"
        assert response2.get('body').get('userId') == test_conversation['users'][0].id

        response3 = await communicator1.receive_json_from()
        assert response3.get('body').get('newStatus') == "offline"
        assert response3.get('type') == "chat.user.changed"
        assert response3.get('body').get('userId') == test_conversation['users'][1].id


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_user_status_changed_with_notification_all_users(test_conversation):
    UserModel = get_user_model()

    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = True
    realtime_chat.consumers.NOTIFY_ALL_USER_STATUS_CHANGE = True
    realtime_chat.services.NOTIFY_ALL_USER_STATUS_CHANGE = True

    user1 = await sync_to_async(UserModel.objects.create)(username=f'ad10', email=f'ad10@ad.ad')
    user2 = await sync_to_async(UserModel.objects.create)(username=f'ad11', email=f'ad11@ad.ad')

    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_conversation['users'][0]

    connected, _ = await communicator.connect()
    assert connected
    await sleep(1)
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_conversation['users'][1]

    connected1, _ = await communicator1.connect()
    assert connected1
    await sleep(1)

    communicator2 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator2.scope['user'] = user1
    connected2, _ = await communicator2.connect()

    communicator3 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator3.scope['user'] = user2
    connected3, _ = await communicator3.connect()

    response1 = await communicator.receive_json_from(1)
    assert response1.get('type') == "chat.user.changed"
    assert response1.get('body').get('newStatus') == "online"
    assert response1.get('body').get('userId') == test_conversation['users'][1].id

    await communicator.disconnect()

    response2 = await communicator1.receive_json_from()
    assert response2.get('type') == "chat.user.changed"

    # response4 = await communicator3.receive_json_from()
    # assert response4.get('body').get('newStatus') == "online"
    # assert response4.get('type') == "chat.user.changed"

    # response4 = await communicator3.receive_json_from()
    # assert response4.get('body').get('newStatus') == "offline"
    # assert response4.get('type') == "chat.user.changed"
