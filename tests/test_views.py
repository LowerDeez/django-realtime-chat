import uuid

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator
from django.contrib.auth import get_user_model
from django.test import Client
from django.urls import reverse
from rest_framework.test import force_authenticate
from rest_framework.test import APIClient

import realtime_chat.consumers
import realtime_chat.conf
from realtime_chat.const import MESSAGE_STATUSES
from realtime_chat.consumers import ChatConsumer
from realtime_chat.models import Message, Conversation, Interlocutor, MessageStatus


@pytest.fixture
@pytest.mark.django_db(transaction=True)
def ws_client():
    def _client(user):
        # c = Client()
        client = APIClient()
        client.force_authenticate(user)
        # c.force_login(user)

        return client

    return _client


@pytest.mark.asyncio
@pytest.mark.django_db(transaction=True)
async def test_delete_message_view(test_conversation, ws_client):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    c = await database_sync_to_async(ws_client)(test_conversation['users'][1])
    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_conversation['users'][0]
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_conversation['users'][1]

    connected, _ = await communicator.connect()
    connected1, _ = await communicator1.connect()

    assert connected
    assert connected1

    response = await database_sync_to_async(c.delete)(
        reverse("chat:destroy_message", kwargs={'pk': str(test_conversation['message'].pk)})
    )
    assert response.status_code == 404

    await database_sync_to_async(c.force_authenticate)(test_conversation['users'][0])
    response = await database_sync_to_async(c.delete)(
        reverse("chat:destroy_message", kwargs={'pk': str(test_conversation['message'].pk)})
    )
    assert response.status_code == 204

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.interlocutor.unread"

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.message.deleted"


@pytest.mark.asyncio
@pytest.mark.django_db(transaction=True)
async def test_update_message_view(test_conversation, ws_client):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    # NOTIFY_ALL_USER_STATUS_CHANGE
    c = await database_sync_to_async(ws_client)(test_conversation['users'][1])
    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_conversation['users'][0]
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_conversation['users'][1]

    connected, _ = await communicator.connect()
    connected1, _ = await communicator1.connect()

    assert connected
    assert connected1

    response = await database_sync_to_async(c.put)(
        reverse("chat:update_message", kwargs={'pk': str(uuid.uuid4())}),
        {},
        format='json'
    )
    assert response.status_code == 404

    await database_sync_to_async(c.force_authenticate)(test_conversation['users'][0])
    response = await database_sync_to_async(c.put)(
        reverse("chat:update_message", kwargs={'pk': str(test_conversation['message'].pk)}),
        data={"content": "new content"},
        format='json'
    )
    assert response.status_code == 200

    response = await communicator1.receive_json_from()
    assert response.get('type') == "chat.message.updated"

    message = await database_sync_to_async(Message.objects.get)(id=test_conversation['message'].pk)
    assert message.text == "new content"


@pytest.mark.django_db(transaction=True)
def test_user_conversation_list(client):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    UserModel = get_user_model()
    user = UserModel.objects.create(
        username="ad",
        email="ad@ad.ad",
        first_name="ad",
        last_name="ad"
    )
    user1 = UserModel.objects.create(
        username="ad1",
        email="ad1@ad.ad",
        first_name="ad1",
        last_name="ad1"
    )
    user2 = UserModel.objects.create(
        username="ad2",
        email="ad2@ad.ad",
        first_name="ad2",
        last_name="ad2"
    )
    user3 = UserModel.objects.create(
        username="ad3",
        email="ad3@ad.ad",
        first_name="ad3",
        last_name="ad3"
    )

    conversation = Conversation.objects.create(owner=user)

    interlocutor = Interlocutor.objects.create(
        user=user,
        conversation=conversation,
        unread=1
    )
    interlocutor1 = Interlocutor.objects.create(
        user=user1,
        conversation=conversation,
        unread=0
    )
    interlocutor2 = Interlocutor.objects.create(
        user=user2,
        conversation=conversation,
        unread=0
    )

    response = client.get(reverse("chat:conversation_list"))
    assert response.status_code == 403

    client.force_login(user)

    response = client.get(reverse("chat:conversation_list"))
    assert response.status_code == 200

    json_data = response.json()

    assert json_data.get('code') == 200
    assert "pagination" in json_data.get('data')
    assert 1 == len(json_data.get('data').get('items'))

    # Interlocutor also has a conversation
    client.force_login(user1)
    response = client.get(reverse("chat:conversation_list"))
    assert response.status_code == 200

    json_data = response.json()

    assert json_data.get('code') == 200
    assert "pagination" in json_data.get('data')
    assert 1 == len(json_data.get('data').get('items'))

    message = Message.objects.create(
        conversation=conversation,
        author=user,
        text="text"
    )
    conversation.last_message = message
    conversation.last_message_at = message.created_at
    conversation.save()

    response = client.get(reverse("chat:conversation_list"))
    json_data = response.json()

    assert "text" == json_data.get('data').get('items')[0].get('latest').get('body').get('content')

    message = Message.objects.create(
        conversation=conversation,
        author=user,
        text="text1"
    )
    conversation.last_message = message
    conversation.last_message_at = message.created_at
    conversation.save()

    response = client.get(reverse("chat:conversation_list"))
    json_data = response.json()

    assert "text1" == json_data.get('data').get('items')[0].get('latest').get('body').get('content')
    assert str(message.id) == json_data.get('data').get('items')[0].get('latest').get('id')

    client.force_login(user3)
    response = client.get(reverse("chat:conversation_list"))
    json_data = response.json()

    assert 0 == len(json_data.get('data').get('items'))


@pytest.mark.django_db(transaction=True)
def test_message_list(client):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    UserModel = get_user_model()
    user = UserModel.objects.create(
        username="ad",
        email="ad@ad.ad",
        first_name="ad",
        last_name="ad"
    )
    user1 = UserModel.objects.create(
        username="ad1",
        email="ad1@ad.ad",
        first_name="ad1",
        last_name="ad1"
    )
    user2 = UserModel.objects.create(
        username="ad2",
        email="ad2@ad.ad",
        first_name="ad2",
        last_name="ad2"
    )

    conversation = Conversation.objects.create(owner=user)

    interlocutor = Interlocutor.objects.create(
        user=user,
        conversation=conversation,
        unread=1
    )
    interlocutor1 = Interlocutor.objects.create(
        user=user1,
        conversation=conversation,
        unread=0
    )
    interlocutor2 = Interlocutor.objects.create(
        user=user2,
        conversation=conversation,
        unread=0
    )
    url = reverse("chat:message_list")+f"?conversation_id={str(conversation.id)}"
    response = client.get(url)
    assert response.status_code == 403

    client.force_login(user)

    response = client.get(url)
    assert response.status_code == 200

    json_data = response.json()

    assert json_data.get('code') == 200
    assert "pagination" in json_data.get('data')
    assert 0 == len(json_data.get('data').get('items'))


    message = Message.objects.create(
        conversation=conversation,
        author=user,
        text="text"
    )
    conversation.last_message = message
    conversation.last_message_at = message.created_at
    conversation.save()

    # Interlocutor also has a conversation
    client.force_login(user1)
    response = client.get(url)
    assert response.status_code == 200

    json_data = response.json()

    assert json_data.get('code') == 200
    assert "pagination" in json_data.get('data')
    assert 1 == len(json_data.get('data').get('items'))

    MessageStatus.objects.create(message=message, status=MESSAGE_STATUSES.received, interlocutor=interlocutor1, user=user1)
    MessageStatus.objects.create(message=message, status=MESSAGE_STATUSES.received, interlocutor=interlocutor2, user=user2)

    message = Message.objects.create(
        conversation=conversation,
        author=user,
        text="text"
    )
    conversation.last_message = message
    conversation.last_message_at = message.created_at
    conversation.save()

    response = client.get(url)
    json_data = response.json()

    assert 2 == len(json_data.get('data').get('items')[1].get('statuses'))

    message = Message.objects.create(
        conversation=conversation,
        author=user,
        text="text1"
    )
    conversation.last_message = message
    conversation.last_message_at = message.created_at
    conversation.save()

    response = client.get(url)
    json_data = response.json()

    assert "text1" == json_data.get('data').get('items')[0].get('body').get('content')


@pytest.mark.django_db(transaction=True)
def test_file_upload(client, csv_file):
    realtime_chat.conf.NOTIFY_USER_STATUS_CHANGE = False
    realtime_chat.consumers.NOTIFY_USER_STATUS_CHANGE = False

    UserModel = get_user_model()
    user = UserModel.objects.create(
        username="ad",
        email="ad@ad.ad",
        first_name="ad",
        last_name="ad"
    )
    user1 = UserModel.objects.create(
        username="ad1",
        email="ad1@ad.ad",
        first_name="ad1",
        last_name="ad1"
    )
    user2 = UserModel.objects.create(
        username="ad2",
        email="ad2@ad.ad",
        first_name="ad2",
        last_name="ad2"
    )

    conversation = Conversation.objects.create(owner=user)

    interlocutor = Interlocutor.objects.create(
        user=user,
        conversation=conversation,
        unread=1
    )
    interlocutor1 = Interlocutor.objects.create(
        user=user1,
        conversation=conversation,
        unread=0
    )
    interlocutor2 = Interlocutor.objects.create(
        user=user2,
        conversation=conversation,
        unread=0
    )

    client.force_login(user)
    file_path = csv_file.name
    f = open(file_path, "r")

    url = reverse('chat:send_file', )

    post_data = {
        'file': f,
        'conversation_id': str(conversation.id)
    }
    response = client.post(url, post_data)
    assert response.status_code == 201
