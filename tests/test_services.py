from datetime import datetime

import pytest
from asgiref.sync import sync_to_async
from channels.testing import WebsocketCommunicator
from django.contrib.auth import get_user_model

from realtime_chat.consumers import ChatConsumer
from realtime_chat.models import Conversation, Interlocutor, UserStatus
from realtime_chat.services import \
    get_or_create_conversation_from_interlocutors


@pytest.mark.django_db
def test_get_or_create_conversation_from_interlocutors():
    UserModel = get_user_model()
    user = UserModel.objects.create(username="ad", email="ad@ad.ad")
    user1 = UserModel.objects.create(username="ad1", email="ad1@ad.ad")
    user2 = UserModel.objects.create(username="ad2", email="ad2@ad.ad")
    user3 = UserModel.objects.create(username="ad3", email="ad3@ad.ad")

    # No conversation, one interlocutor
    assert 0 == Conversation.objects.count()
    assert 0 == Interlocutor.objects.count()
    first_conversation, created = get_or_create_conversation_from_interlocutors(
        user, user1.id
    )
    assert 1 == Conversation.objects.count()
    c = Interlocutor.objects.filter(conversation=first_conversation).count()
    assert 2 == c

    # Same conversation with the same interlocutors
    conversation, created = get_or_create_conversation_from_interlocutors(user, user1.id)
    assert 1 == Conversation.objects.count()
    assert first_conversation.id == conversation.id
    assert (
        2
        == Interlocutor.objects.filter(conversation=first_conversation).count()
    )

    # No conversation, multiple users
    second_conversation, created = get_or_create_conversation_from_interlocutors(
        user, user1.id, user2.id, user3.id
    )
    assert 2 == Conversation.objects.count()
    assert (
        4
        == Interlocutor.objects.filter(conversation=second_conversation).count()
    )

    # Different users, new conversation
    third_conversation, created = get_or_create_conversation_from_interlocutors(
        user, user2.id
    )
    assert 3 == Conversation.objects.count()
    assert (
        2
        == Interlocutor.objects.filter(conversation=third_conversation).count()
    )

    # Different order, no new conversation
    fourth_conversation, created = get_or_create_conversation_from_interlocutors(
        user1, user.id, user2.id, user3.id
    )
    assert 3 == Conversation.objects.count()
    assert second_conversation.id == fourth_conversation.id
    assert (
        4
        == Interlocutor.objects.filter(conversation=fourth_conversation).count()
    )


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_chat_consumers_invalid_data(test_users):
    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_users[0]

    connected, _ = await communicator.connect()
    assert connected

    status = await sync_to_async(UserStatus.objects.get)(user=test_users[0])
    assert status.is_online

    await communicator.disconnect()

    status = await sync_to_async(UserStatus.objects.get)(user=test_users[0])
    assert not status.is_online

    # Two clients were connected
    communicator = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator.scope['user'] = test_users[0]

    connected, _ = await communicator.connect()
    assert connected
    communicator1 = WebsocketCommunicator(ChatConsumer, "/testws/")
    communicator1.scope['user'] = test_users[0]

    connected, _ = await communicator1.connect()
    assert connected

    status = await sync_to_async(UserStatus.objects.get)(user=test_users[0])
    assert status.is_online

    await communicator.disconnect()

    status = await sync_to_async(UserStatus.objects.get)(user=test_users[0])
    assert status.is_online

    await communicator1.disconnect()

    status = await sync_to_async(UserStatus.objects.get)(user=test_users[0])
    assert not status.is_online
