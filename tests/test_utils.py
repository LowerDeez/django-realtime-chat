from datetime import datetime

import pytest
from django.contrib.auth import get_user_model

from realtime_chat.models import Conversation, Interlocutor
from realtime_chat.utils import (format_websocket_message,
                                 get_user_group_channel_name)


@pytest.mark.django_db
def test_get_user_group_channel_name():
    UserModel = get_user_model()
    user = UserModel.objects.create(username="ad", email="ad@ad.ad")

    assert f"channel__{user.id}" == get_user_group_channel_name(user)
    assert "channel__3" == get_user_group_channel_name(3)


def test_format_websocket_message():
    message = format_websocket_message("type", {"text": "text"})

    assert message["created_at"].endswith("Z")
