#### Development

To start the development on this project

1. Fork repository.

2. Clone your fork.

3. Create `.env` file from `.env.example`.

4. Run docker-compose with ``docker-compose up --build``.

5. Open ``<your_ip_address>:8535`` in your browser. Now you can work on it like with a regular django/python project.

6. Modify code.

7. Write tests for your changes.

8. Submit merge request.
