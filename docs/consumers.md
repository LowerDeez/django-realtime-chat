### Chat consumers

There is one consumer that handles all chat messages.

Thus if you want to write your own a very custom consumer - don't include it in your `routing.py`, instead create your own based on `ChatConsumer`.

If you override consumer with your own it's important to handle user status change and message events processing.
