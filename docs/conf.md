# Settings

- **CHAT_IMAGE_EXTENSIONS** - File extensions to identify that file is image.
  Default - `[".png", ".jpg", ".jpeg", ".bmp", ".gif"]`.

- **CHAT_MESSAGE_PREFIX** - Prefix used as namespace for events and messages. Used in channels messages as `type`. Default - `chat.`.

- **CHAT_SEND_MESSAGE_WITHOUT_HANDLER** - django-channels' consumers must have a special method(handler) for each message `type` to propagate message to user. If `True` all messages without handler will be send as they are, otherwise raises ValueError. Default - `True`.

- **CHAT_EXTRA_EVENT_HANDLERS** - dictionary with websocket event handlers, where key is a message type without `MESSAGE_PREFIX` and value is an async callable(as a string). Used to process message before sending it to a concrete user. Check [examples](chat_handler.md) for more details.

- **CHAT_EXTRA_MESSAGE_HANDLERS** - dictionary with websocket message handlers, where key is a message type without `MESSAGE_PREFIX` and value is an async callable. Used to handle received message from a client. Check [examples](chat_handler.md) for more details

- **CHAT_MESSAGE_RENDERER** - JSON encoder class used to encode messages in consumer. Default - `"realtime_chat.utils.JSONRenderer"`

- **CHAT_MESSAGE_PARSER** - JSON decoder class used to decode messages in consumer. Default - "realtime_chat.utils.JSONParser"

- **CHAT_FILE_VALIDATORS** - list of drf validators(as strings). For more details check [drf documentation](https://www.django-rest-framework.org/api-guide/validators/#writing-custom-validators). Default - []

- **CHAT_MESSAGE_SERIALIZER** - websocket message serializer, used to validate message before passing it to handler. Default - `"realtime_chat.serializers.WSMessageSerializer"`

- **CHAT_NOTIFY_USER_STATUS_CHANGE** - Enables notifications about users' online status change. Default - `True`

- **CHAT_NOTIFY_ALL_USER_STATUS_CHANGE** - Controls whether to send message about user online status change to all users or only user interlocutors. *NOTE: may lead to performance problems with large user database if enabled*. Default - `False`

#### Message types

List of websocket messages received from client:

- `typing.start`: Chat interlocutor starts typing a message.
- `typing.stop`: Chat interlocutor stops typing a message.
- `message.created`: A new message from one of the interlocutors.
- `message.read`: Message was read by one of the interlocutors.
- `message.received`: Message was received by one of the interlocutors.

Websocket messages that are sent only from server:

- `user.changed`: User status was changed e.g. user become offline.
- `conversation.new`: User sent a new message which caused a new conversation.
- `message.deleted`: Message was deleted by the message author.
