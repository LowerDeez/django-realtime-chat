#### Chat handlers

There is a list of predefined chat events that you can see in [conf.py](conf.md)

With **CHAT_EXTRA_EVENT_HANDLERS** dictionary you can define your own set of chat events or override default handlers.

Handler must be an async callable that receives consumers scope and validated message data.

The most basic example:

```python
from channels.layers import get_channel_layer

from realtime_chat.utils import get_user_group_channel_name, format_websocket_message

async def event_handler(scope, type, body, **kwargs):
    validate_message(type, body, kwargs)

    '''Do some logic on your message'''

    # Send response to a clients
    channel_layer = get_channel_layer()
    await channel_layer.group_send(
        get_user_group_channel_name(conversation_interlocutor_user_id),
        format_websocket_message(response_type, response_body)
    )
```

For more examples check `chat_handlers.py` file.
