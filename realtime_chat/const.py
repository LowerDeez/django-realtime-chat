from django.utils.translation import gettext_lazy as _
from model_utils.choices import Choices


MESSAGE_STATUSES = Choices(
    (0, "created", _('Created')),
    (1, "received", _('Received')),
    (2, "read", _('Read')),
    (3, "deleted", _('Deleted')),
)
