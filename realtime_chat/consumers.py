import asyncio

from asgiref.sync import sync_to_async
from channels.consumer import get_handler_name
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.utils.module_loading import import_string
from django.utils.translation import gettext

from .conf import (
    MESSAGE_RENDERER,
    MESSAGE_PARSER,
    MESSAGE_PREFIX,
    SEND_MESSAGE_WITHOUT_HANDLER,
    MESSAGE_SERIALIZER,
    MESSAGE_HANDLERS,
    EVENT_HANDLERS,
    NOTIFY_USER_STATUS_CHANGE
)
from .services import user_connected, user_disconnected, notify_user_status_changed
from .utils import (
    format_websocket_message,
    get_user_group_channel_name
)


class ChatConsumer(AsyncJsonWebsocketConsumer):
    """
    Chat consumer.

    Attributes:
        message_renderer: Message json encoder class.
        message_parser: Message json decoder class.
        message_serializer: Message serializer to validate data and pass it to handler.
    """

    message_renderer = import_string(MESSAGE_RENDERER)()
    message_parser = import_string(MESSAGE_PARSER)()
    message_serializer = import_string(MESSAGE_SERIALIZER)

    async def connect(self):
        # NOTE: user is always connected to his chat consumer though
        # he receives all the messages from all chats always, even if he is
        # not inside chat view. This is done because we want to check user
        # online status and client must have a way notify about messages in
        # different places.

        # TODO: is there a need of a more advanced way of registering user for
        # receiving new messages to reduce network usage if not required
        # Mark user as online
        status_changed = await sync_to_async(user_connected)(self.scope["user"])

        await self.accept()

        # The only way by now to send messages from one user to another
        # is to create a separate group for each user
        await self.channel_layer.group_add(
            group=get_user_group_channel_name(self.scope["user"]),
            channel=self.channel_name,
        )

        if status_changed and NOTIFY_USER_STATUS_CHANGE:
            await sync_to_async(notify_user_status_changed)(self.scope["user"].id, 'online')


    @classmethod
    async def decode_json(cls, text_data):
        try:
            return cls.message_parser.parse(text_data)
        except Exception as e:
            raise ValueError(gettext("Cannot parse message"))

    @classmethod
    async def encode_json(cls, content):
        try:
            return cls.message_renderer.render(content)
        except Exception as e:
            raise ValueError(gettext("Cannot encode message"))

    async def get_event_handler(self, type: str):
        return EVENT_HANDLERS.get(type)

    async def receive_json(self, content, **kwargs):
        try:
            serializer = self.message_serializer(
                data=content, context={"scope": self.scope}
            )
            serializer.is_valid(raise_exception=True)

            event_handler = await self.get_event_handler(
                serializer.data["type"]
            )
            await event_handler(
                scope=self.scope,
                **serializer.data,
            )
        except asyncio.CancelledError as e:
            raise e
        except Exception as e:
            # TODO: add custom exception with custom exception handling
            await self.send_json(
                format_websocket_message(
                    "error", {"domain": "request", "message": str(e)},
                )
            )
            return

    async def dispatch(self, message):
        handler_name = get_handler_name(message)
        external_handler = MESSAGE_HANDLERS.get(
            handler_name[len(MESSAGE_PREFIX) :]
        )

        if external_handler:
            await external_handler(message, self)
            return

        # It can be a default event - try to process it
        handler = getattr(self, handler_name, None)

        if handler:
            await handler(message)
            return

        if SEND_MESSAGE_WITHOUT_HANDLER:
            await self.send_json(message)
        else:
            raise ValueError("No handler for message type %s" % message["type"])

    async def disconnect(self, close_code):
        # Mark user as offline
        status_changed = await sync_to_async(user_disconnected)(self.scope["user"])

        if status_changed and NOTIFY_USER_STATUS_CHANGE:
            await sync_to_async(notify_user_status_changed)(self.scope["user"].id, 'offline')
