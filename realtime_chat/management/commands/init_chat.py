from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from realtime_chat.models import UserStatus


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Create statuses for all users to prevent extra query
        UserModel = get_user_model()

        UserStatus.objects.bulk_create(
            [
                UserStatus(user=user)
                for user in UserModel.objects.filter(status__isnull=True)
            ]
        )
