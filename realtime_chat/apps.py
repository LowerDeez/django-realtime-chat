from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class DjangoRealtimeChatConfig(AppConfig):
    name = "realtime_chat"
    verbose_name = _("Realtime chat")

    def ready(self):
        from .handlers import create_user_status, load_settings
        load_settings()
