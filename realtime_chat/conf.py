from typing import Callable, Dict, List

from django.conf import settings

__all__ = (
    "EXTRA_EVENT_HANDLERS",
    "EXTRA_MESSAGE_HANDLERS",
    "IMAGE_EXTENSIONS",
    "MESSAGE_PREFIX",
    "SEND_MESSAGE_WITHOUT_HANDLER",
    "MESSAGE_RENDERER",
    "MESSAGE_PARSER",
    "FILE_VALIDATORS",
    "USER_EXTRA_FIELDS",
    "USER_AVATAR_FIELD",
    "EVENT_HANDLERS",
    "MESSAGE_HANDLERS",
    "NOTIFY_USER_STATUS_CHANGE",
    "NOTIFY_ALL_USER_STATUS_CHANGE",
    "MESSAGE_SERIALIZER"
)

EXTRA_EVENT_HANDLERS: Dict[str, Callable] = getattr(
    settings, "CHAT_EXTRA_EVENT_HANDLERS", {}
)
EXTRA_MESSAGE_HANDLERS: Dict[str, Callable] = getattr(
    settings, "CHAT_EXTRA_MESSAGE_HANDLERS", {}
)
IMAGE_EXTENSIONS: List[str] = getattr(
    settings, "CHAT_IMAGE_EXTENSIONS", [".png", ".jpg", ".jpeg", ".bmp", ".gif"]
)
MESSAGE_PREFIX: str = getattr(settings, "CHAT_MESSAGE_PREFIX", "chat")
SEND_MESSAGE_WITHOUT_HANDLER: bool = getattr(
    settings, "CHAT_SEND_MESSAGE_WITHOUT_HANDLER", True
)
MESSAGE_RENDERER: str = getattr(
    settings, "CHAT_MESSAGE_RENDERER", "realtime_chat.utils.JSONRenderer"
)
MESSAGE_PARSER: str = getattr(
    settings, "CHAT_MESSAGE_PARSER", "realtime_chat.utils.JSONParser"
)
FILE_VALIDATORS: List[str] = getattr(settings, "CHAT_FILE_VALIDATORS", [])
MESSAGE_SERIALIZER: str = getattr(
    settings, "CHAT_MESSAGE_SERIALIZER", "realtime_chat.serializers.WSMessageSerializer"
)
NOTIFY_USER_STATUS_CHANGE: bool = getattr(
    settings, "CHAT_NOTIFY_USER_STATUS_CHANGE", True
)
NOTIFY_ALL_USER_STATUS_CHANGE: bool = getattr(
    settings, "CHAT_NOTIFY_ALL_USER_STATUS_CHANGE", False
)
USER_AVATAR_FIELD: str = getattr(
    settings, "CHAT_USER_AVATAR_FIELD", ''
)
USER_EXTRA_FIELDS: List[str] = getattr(
    settings, "REALTIME_CHAT_USER_EXTRA_FIELDS", ["email", "avatar"]
)

EVENT_HANDLERS = {
    "typing.start": "realtime_chat.chat_handlers.SimpleEventPropagator",
    "typing.stop": "realtime_chat.chat_handlers.SimpleEventPropagator",
    # "conversation.new": None, # no handler as client never sends it
    "message.created": "realtime_chat.chat_handlers.NewMessageHandler",
    "message.read": "realtime_chat.chat_handlers.ReadMessageHandler",
    "message.received": "realtime_chat.chat_handlers.ReceiveMessageHandler",
    # "user.changed": None, #  no handler as client never send it
    **EXTRA_EVENT_HANDLERS,
}
MESSAGE_HANDLERS = {
    **EXTRA_MESSAGE_HANDLERS,
}
