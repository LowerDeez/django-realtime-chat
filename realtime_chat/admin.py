from django.contrib import admin

from . import models


@admin.register(models.Conversation)
class ConversationAdmin(admin.ModelAdmin):
    list_display = ['owner', 'last_message_at', 'created_at']
    autocomplete_fields = ['owner']
    search_fields = ['id']
    list_select_related = ['owner']


@admin.register(models.Interlocutor)
class InterlocutorAdmin(admin.ModelAdmin):
    list_display = ['id', 'user_id', 'conversation', 'unread']
    search_fields = ['id', 'user__username', 'user__email']
    list_select_related = ['conversation']
    autocomplete_fields = ['user', 'conversation']


class MessageAdminInline(admin.TabularInline):
    model = models.MessageStatus
    extra = 0
    readonly_fields = ['user', 'interlocutor']


@admin.register(models.Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ['id', 'author', 'text', 'created_at', 'edited_at', 'deleted_at']
    list_select_related = ['author']
    autocomplete_fields = ['author', 'conversation']
    inlines = [MessageAdminInline]


@admin.register(models.MessageStatus)
class MessageStatusAdmin(admin.ModelAdmin):
    pass


@admin.register(models.UserStatus)
class UserStatusAdmin(admin.ModelAdmin):
    list_display = ['user', 'is_online', 'changed_at']
    search_fields = ['user__username']
    list_select_related = ['user']
    list_filter = ['is_online']
