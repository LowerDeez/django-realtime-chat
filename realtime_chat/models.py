from typing import Optional
from uuid import uuid4

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from model_utils.fields import AutoLastModifiedField

from .conf import IMAGE_EXTENSIONS
from .const import MESSAGE_STATUSES
from .querysets import ConversationQuerySet, InterlocutorQuerySet

__all__ = (
    "Message",
    "Conversation",
    "Interlocutor",
    "MessageStatus",
    "UserStatus",
)


class Message(models.Model):
    id = models.UUIDField(verbose_name=_("ID"), default=uuid4, primary_key=True)

    conversation = models.ForeignKey(
        "realtime_chat.Conversation",
        verbose_name=_("Message conversation"),
        on_delete=models.CASCADE,
        null=True,
        related_name="conversation_messages",
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Message author"),
        on_delete=models.CASCADE,
        null=True,
    )
    text = models.TextField(
        verbose_name=_("Message text body"), blank=True, null=True
    )
    content_type = models.CharField(
        verbose_name=_('File content type'),
        max_length=255,
        blank=True,
        null=True
    )
    file = models.FileField(
        verbose_name=_("Message file"), blank=True, null=True
    )
    created_at = models.DateTimeField(
        verbose_name=_("Message created at"), default=timezone.now
    )
    edited_at = models.DateTimeField(
        verbose_name=_("Message changed at"), blank=True, null=True
    )
    deleted_at = models.DateTimeField(
        verbose_name=_("Message deleted at"), blank=True, null=True
    )

    class Meta:
        verbose_name = _("Chat message")
        verbose_name_plural = _("Chat messages")
        ordering = ["-created_at"]

    def __str__(self) -> str:
        return str(self.id)

    @property
    def is_image(self) -> bool:
        return bool(
            self.file
            and self.file.name.lower().endswith(tuple(IMAGE_EXTENSIONS))
        )

    def process_delete(self):
        self.text = ""

        if self.file:
            self.file.delete()


class MessageStatus(models.Model):
    message = models.ForeignKey(
        "realtime_chat.Message",
        verbose_name=_("Conversation message"),
        on_delete=models.CASCADE,
        related_name="statuses",
    )
    interlocutor = models.ForeignKey(
        "realtime_chat.Interlocutor",
        verbose_name=_("Conversation interlocutor"),
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Conversation interlocutor user"),
        on_delete=models.CASCADE,
        null=True,
    )
    status = models.PositiveSmallIntegerField(
        verbose_name=_("Message status"), choices=MESSAGE_STATUSES,
    )
    created_at = models.DateTimeField(
        verbose_name=_("Status set at"), default=timezone.now
    )

    class Meta:
        verbose_name = _("Message status")
        verbose_name_plural = _("Message statuses")

    def __str__(self) -> str:
        return f"{self.status} at {self.created_at}"

    def get_status_python(self) -> Optional[str]:
        current_status = (
            python
            for db, python, verbose in MESSAGE_STATUSES._triples
            if db == self.status
        )

        return next(iter(current_status), None)


class Conversation(models.Model):
    id = models.UUIDField(verbose_name=_("ID"), default=uuid4, primary_key=True)
    title = models.CharField(
        verbose_name=_("Conversation title"),
        max_length=255,
        blank=True,
        null=True,
    )
    image = models.ImageField(
        verbose_name=_("Conversation avatar"), blank=True, null=True
    )
    last_message = models.OneToOneField(
        Message,
        verbose_name=_("Last message"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="message_conversation",
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Conversation owner"),
        null=True,
        on_delete=models.CASCADE,
        related_name="owned_conversations",
    )
    last_message_at = models.DateTimeField(
        verbose_name=_("Last message's was created at"), blank=True, null=True
    )
    created_at = models.DateTimeField(
        verbose_name=_("Created at"), default=timezone.now
    )

    objects = ConversationQuerySet().as_manager()

    class Meta:
        verbose_name = _("Chat conversation")
        verbose_name_plural = _("Chat conversations")

    def __str__(self) -> str:
        return str(self.id)


class Interlocutor(models.Model):
    id = models.UUIDField(verbose_name=_("ID"), default=uuid4, primary_key=True)

    conversation = models.ForeignKey(
        Conversation,
        verbose_name=_("Interlocutor's conversation"),
        related_name="interlocutors",
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Interlocutor's user"),
        related_name="conversation_interlocutors",
        on_delete=models.CASCADE,
    )
    unread = models.PositiveIntegerField(
        verbose_name=_("Unread messages"), default=0
    )

    created_at = models.DateTimeField(
        verbose_name=_("Interlocutor joined at"), default=timezone.now
    )

    objects = InterlocutorQuerySet().as_manager()

    class Meta:
        verbose_name = _("Chat interlocutor")
        verbose_name_plural = _("Chat interlocutors")

    def __str__(self) -> str:
        return str(self.id)


class UserStatus(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        verbose_name=_("User status"),
        related_name="status",
        on_delete=models.CASCADE,
    )
    is_online = models.BooleanField(verbose_name=_("Is online"), default=False)
    changed_at = AutoLastModifiedField(verbose_name=_("Changed at"))

    class Meta:
        verbose_name = _("User status")
        verbose_name_plural = _("User statuses")

    @property
    def status(self):
        return "online" if self.is_online else "offline"

    def __str__(self) -> str:
        return gettext("User is {} from {}").format(
            self.status, self.changed_at.strftime("%Y-%m-%d %H:%M:%s")
        )
