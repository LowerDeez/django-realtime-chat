from typing import Dict, List

import bleach
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.module_loading import import_string
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from standards.drf.serializers import ModelSerializer, Serializer
from drf_yasg.utils import swagger_serializer_method

from . import models
from .conf import FILE_VALIDATORS, EVENT_HANDLERS, USER_AVATAR_FIELD, USER_EXTRA_FIELDS
from .utils import get_event_name_without_prefix
from .const import MESSAGE_STATUSES

UserModel = get_user_model()


class UserStatusSerializer(ModelSerializer):
    type = serializers.SerializerMethodField()

    class Meta:
        model = models.UserStatus
        fields = ["type", "changed_at"]

    def get_type(self, obj: models.UserStatus) -> str:
        return obj.status


class InterlocutorSerializer(ModelSerializer):
    display_name = serializers.SerializerMethodField()
    nick_name = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = models.Interlocutor
        fields = [
            "id",
            "user_id",
            "nick_name",
            "display_name",
            "first_name",
            "last_name",
            "status",
            "unread",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if USER_AVATAR_FIELD and hasattr(UserModel, USER_AVATAR_FIELD):
            self.fields.update(
                {
                    "avatar": serializers.SerializerMethodField(
                        method_name="get_avatar"
                    )
                }
            )

    def get_avatar(self, obj: models.Interlocutor) -> str:
        avatar = getattr(obj.user, USER_AVATAR_FIELD, "")

        if avatar and avatar.url:
            return avatar.url

        return ""

    def get_display_name(self, obj: models.Interlocutor) -> str:
        if hasattr(obj.user, "get_display_name"):
            return obj.user.get_display_name()

        return obj.user.get_full_name() or obj.user.username

    def get_nick_name(self, obj: models.Interlocutor) -> str:
        return getattr(obj.user, obj.user.USERNAME_FIELD, "")

    def get_first_name(self, obj: models.Interlocutor) -> str:
        return getattr(obj.user, "first_name", "")

    def get_last_name(self, obj: models.Interlocutor) -> str:
        return getattr(obj.user, "last_name", "")

    def get_status(self, obj: models.Interlocutor):
        try:
            return UserStatusSerializer(instance=obj.user.status).data
        except models.UserStatus.DoesNotExist:
            return {}


class MessageTextBodySerializer(ModelSerializer):
    content = serializers.SerializerMethodField()

    class Meta:
        model = models.Message
        fields = ["content"]

    def get_content(self, obj: models.Message):
        return obj.text


class MessageFileBodySerializer(ModelSerializer):
    name = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    size = serializers.SerializerMethodField()
    mime_type = serializers.SerializerMethodField()
    source = serializers.SerializerMethodField()

    class Meta:
        model = models.Message
        fields = ["name", "type", "size", "source", "mime_type"]

    def get_name(self, obj: models.Message) -> str:
        return obj.file.name

    def get_type(self, obj: models.Message) -> bool:
        return obj.is_image

    def get_size(self, obj: models.Message) -> int:
        return obj.file.size

    def get_source(self, obj: models.Message) -> str:
        return obj.file.url

    def get_mime_type(self, obj: models.Message) -> str:
        return obj.content_type


class MessageStatusSerializer(ModelSerializer):
    changed_at = serializers.DateTimeField(source="created_at")
    interlocutor_id = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = models.MessageStatus
        fields = ["interlocutor_id", "user_id", "status", "changed_at"]

    def get_interlocutor_id(self, obj: models.MessageStatus):
        return str(obj.interlocutor_id)

    @swagger_serializer_method(
        serializer_or_field=serializers.ChoiceField(
            choices=tuple(_[1] for _ in MESSAGE_STATUSES._triples)
        )
    )
    def get_status(self, obj: models.MessageStatus):
        return obj.get_status_python()


class MessageSerializer(ModelSerializer):
    body = serializers.SerializerMethodField()
    conversation_id = serializers.SerializerMethodField()
    statuses = serializers.SerializerMethodField()

    class Meta:
        model = models.Message
        fields = [
            "id",
            "author_id",
            "conversation_id",
            "body",
            "statuses",
            "created_at",
            "edited_at",
            "deleted_at",
        ]

    def get_conversation_id(self, obj: models.Message) -> str:
        return str(obj.conversation_id)

    def get_body(self, obj: models.Message) -> Dict:
        if obj.text:
            return MessageTextBodySerializer(obj).data
        else:
            return MessageFileBodySerializer(obj).data

    @swagger_serializer_method(serializer_or_field=serializers.ListField(child=MessageStatusSerializer()))
    def get_statuses(self, obj: models.Message) -> List[Dict]:
        return [
            MessageStatusSerializer(instance=status).data
            for status in obj.statuses.all()
        ]


class LatestMessageSerializer(ModelSerializer):
    body = serializers.SerializerMethodField()
    conversation_id = serializers.SerializerMethodField()

    class Meta:
        model = models.Message
        fields = [
            "id",
            "author_id",
            "conversation_id",
            "body",
            "created_at",
            "edited_at",
            "deleted_at",
        ]

    def get_conversation_id(self, obj: models.Message) -> str:
        return str(obj.conversation_id)

    def get_body(self, obj: models.Message) -> Dict:
        if obj.text:
            return MessageTextBodySerializer(obj).data
        else:
            return MessageFileBodySerializer(obj).data


class ConversationSerializer(ModelSerializer):
    interlocutors = serializers.ListSerializer(child=InterlocutorSerializer())
    latest = serializers.SerializerMethodField()

    class Meta:
        model = models.Conversation
        fields = [
            "id",
            "title",
            "latest",
            "image",
            "interlocutors",
            "owner_id",
        ]

    def get_latest(self, obj: models.Conversation):
        if obj.last_message:
            return MessageSerializer(obj.last_message).data

        return {}


class UserSearchSerializer(ModelSerializer):
    class Meta:
        model = UserModel
        fields = [
            "id",
            UserModel.USERNAME_FIELD,
            *USER_EXTRA_FIELDS,
        ]


class NewMessageSerializer(ModelSerializer):
    interlocutors = serializers.ListField(
        child=serializers.IntegerField(), required=False
    )
    conversation_id = serializers.UUIDField(required=False)

    class Meta:
        model = models.Message
        fields = ["conversation_id", "interlocutors"]

    def validate(self, data):
        conversation = data.get("conversation_id")
        interlocutors = data.get("interlocutors")

        if not conversation and not interlocutors:
            raise serializers.ValidationError(
                _(
                    "You need to provide either conversation or interlocutors to send a message"
                )
            )

        return data

    def validate_conversation_id(self, value):
        # No conversation_id, probably interlocutors are sent
        if not value:
            return value

        user_in_chat = models.Interlocutor.objects.filter(
            conversation_id=value, user_id=self.context["user"]
        )

        if not user_in_chat.exists():
            raise serializers.ValidationError(
                gettext("No such chat or you are not allowed to write in it")
            )

        return value

    def validate_interlocutors(self, value):
        # Only unique values are allowed
        if value:
            return list(set(value))

        return value


class NewTextMessageSerializer(NewMessageSerializer):
    content = serializers.CharField()

    class Meta:
        model = models.Message
        fields = ["conversation_id", "content", "interlocutors"]

    def validate_content(self, value: str):
        if not value:
            raise serializers.ValidationError(_("Text cannot be empty"))

        has_not_only_whitespaces = any([char != " " for char in value])

        if not has_not_only_whitespaces:
            raise serializers.ValidationError(
                _("Text cannot have only whitespaces")
            )

        return bleach.clean(value)

    def get_message_instance(self, data):
        author = self.context["user"]
        message = models.Message(
            conversation_id=data.get("conversation_id"),
            author=author,
            text=data.get("content"),
        )

        return message


class NewFileMessageSerializer(NewMessageSerializer):
    file = serializers.FileField()

    class Meta:
        model = models.Message
        fields = ["conversation_id", "file", "interlocutors"]

    def validate_file(self, value):
        validators = [import_string(validator) for validator in FILE_VALIDATORS]

        for validator in validators:
            try:
                validator()(value, self)
            except serializers.ValidationError as e:
                raise e
            except Exception as e:
                raise serializers.ValidationError(str(e))

        return value

    def get_message_instance(self, data):
        author = self.context["user"]
        message = models.Message(
            conversation_id=data.get("conversation_id"),
            author=author,
            file=data.get("file"),
            content_type=data.get("file").content_type,
        )
        return message


class TypingBodySerializer(Serializer):
    conversation_id = serializers.UUIDField()


class TypingEventSerializer(Serializer):
    type = serializers.CharField()
    created_at = serializers.DateTimeField()
    body = TypingBodySerializer()

    class Meta:
        fields = ["type", "created_at", "body"]


class InterlocutorUnreadSerializer(ModelSerializer):
    conversation_id = serializers.SerializerMethodField()

    class Meta:
        model = models.Interlocutor
        fields = ["id", "user_id", "conversation_id", "unread"]

    def get_conversation_id(self, obj: models.Interlocutor) -> str:
        return str(obj.conversation_id)


class MessageStatusChangeSerializer(Serializer):
    message_id = serializers.UUIDField()
    conversation_id = serializers.UUIDField()

    class Meta:
        fields = ["message_id", "conversation_id"]

    def validate(self, data) -> Dict:
        message_id = data.get("message_id")
        conversation_id = data.get("conversation_id")

        message = models.Message.objects.filter(
            id=message_id, conversation_id=conversation_id
        ).first()

        if not message:
            raise serializers.ValidationError(gettext("Invalid message"))

        status = models.MessageStatus.objects.filter(
            message_id=message_id, user=self.context["scope"].get("user")
        ).first()

        if status and status.status < self.context["new_status"]:
            raise serializers.ValidationError(gettext("Cannot change status"))

        return data

    def validate_conversation_id(self, value):
        user_in_chat = models.Interlocutor.objects.filter(
            conversation_id=value, user_id=self.context["user"].id
        )

        if not user_in_chat.exists():
            raise serializers.ValidationError(
                _("No such chat or you are not allowed to write in it")
            )

        return value


class MessageUpdateSerializer(ModelSerializer):
    conversation_id = serializers.SerializerMethodField()
    content = serializers.CharField(source="text")

    class Meta:
        model = models.Message
        fields = ["id", "content", "conversation_id"]

    def get_conversation_id(self, obj: models.Message):
        return obj.conversation_id


class WSMessageSerializer(Serializer):
    type = serializers.CharField()
    created_at = serializers.DateTimeField()
    body = serializers.DictField()

    def validate_type(self, value: str) -> str:
        event_without_prefix = get_event_name_without_prefix(value)
        self.event_handler = EVENT_HANDLERS.get(event_without_prefix)

        if not self.event_handler:
            raise serializers.ValidationError(
                gettext(
                    "Unsupported event type: {event_type} "
                    "searched as {search_value}"
                ).format(event_type=value, search_value=event_without_prefix)
            )

        return event_without_prefix


class UserEventMessageSerializer(Serializer):
    conversation_id = serializers.CharField()
    user_id = serializers.SerializerMethodField()

    def get_user_id(self, obj: Dict) -> int:
        return self.context["scope"]["user"].id

    def validate_conversation_id(self, value: str) -> str:
        if not value:
            # Check whether he goes here
            raise serializers.ValidationError(
                gettext("conversation_id is required to pass user event")
            )

        return value


class MessageStatusUpdateSerializer(Serializer):
    message_id = serializers.CharField()
    conversation_id = serializers.CharField()
    user_id = serializers.IntegerField()
    interlocutor_id = serializers.UUIDField()


class UserStatusChangedSerializer(Serializer):
    user_id = serializers.IntegerField()
    new_status = serializers.CharField()
    changed_at = serializers.DateTimeField()
